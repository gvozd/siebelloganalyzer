package com.extremumlogica.siebellog.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogLine {

	private String type;
	private String internalType;
	private String subType;
	private int severity;
	private String sarmId;
	private LocalDateTime date;
	private String message;
	private String extraInfo;
	private boolean warning;
	private boolean error;
	private boolean sectionStart;
	private boolean sectionEnd;
	private long id;
	private String address;
	private ArrayList<LogLine> items;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType
	 *            the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the severity
	 */
	public int getSeverity() {
		return severity;
	}

	/**
	 * @param severity
	 *            the severity to set
	 */
	public void setSeverity(int severity) {
		this.severity = severity;
	}

	/**
	 * @return the sarmId
	 */
	public String getSarmId() {
		return sarmId;
	}

	/**
	 * @param sarmId
	 *            the sarmId to set
	 */
	public void setSarmId(String sarmId) {
		this.sarmId = sarmId;
	}

	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the extraInfo
	 */
	public String getExtraInfo() {
		return extraInfo;
	}

	/**
	 * @param extraInfo
	 *            the extraInfo to set
	 */
	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	/**
	 * @return the warning
	 */
	public boolean isWarning() {
		return warning;
	}

	/**
	 * @param warning
	 *            the warning to set
	 */
	public void setWarning(boolean warning) {
		this.warning = warning;
	}

	/**
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(boolean error) {
		this.error = error;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the items
	 */
	public ArrayList<LogLine> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(ArrayList<LogLine> items) {
		this.items = items;
	}

	/**
	 * @return the sectionStart
	 */
	public boolean isSectionStart() {
		return sectionStart;
	}

	/**
	 * @return the sectionEnd
	 */
	public boolean isSectionEnd() {
		return sectionEnd;
	}

	/**
	 * @return the Internal Type of the log line
	 */
	public String getInternalType() {
		return internalType;
	}

	private void getLineAction() {
		switch (type) {
		case "PrcExec":
			switch (subType) {
			case "PrcExec":
				sectionStart = true;
				break;
			case "End":
				sectionEnd = true;
				break;
			}
			internalType = "workflow";
			break;
		case "StpExec":
			switch (subType) {
			case "Create":
				sectionStart = true;
				break;
			case "End":
				sectionEnd = true;
				break;
			}
			internalType = "workflow_step";
			break;
		case "ObjMgrBusServiceLog":
			if (subType.matches("InvokeMethod")) {
				if (message
						.matches("^Begin: Business Service '([\\s\\S]+)' invoke method: '([\\s\\S]+)' at ([\\s\\S]+)$")) {
					sectionStart = true;
				}
				if (message
						.matches("^End: Business Service '([\\s\\S]+)' invoke method: '([\\s\\S]+)' at ([\\s\\S]+)$")) {
					sectionEnd = true;
				}
			}
			internalType = "business_service";
			break;
		case "ObjMgrSRFLog":
			if (subType.matches("Open")) {
				if (message.matches("Begin: OpenSRF")) {
					sectionStart = true;
				}
				if (message.matches("End: OpenSRF")) {
					sectionEnd = true;
				}

			}
			break;
		case "ObjMgrLicense":
			if (subType.matches("Load")) {
				if (message.matches("Begin: LoadLicenseManager")) {
					sectionStart = true;
				}
				if (message.matches("End: LoadLicenseManager")) {
					sectionEnd = true;
				}
			}
			break;
		case "ObjMgrSqlObjLog":
			switch (subType) {
			case "Create":
				sectionStart = message
						.matches("^Begin: construct SqlObj([\\s\\S]{0,3}) '([\\s\\S]+)' at ([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: construct SqlObj([\\s\\S]{0,3}) '([\\s\\S]+)' at ([\\s\\S]+)$");
				break;
			case "Release":
				sectionStart = message
						.matches("^Begin: Release SqlObj([\\s\\S]{0,3}) '([\\s\\S]+)' at ([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: Release SqlObj([\\s\\S]{0,3}) '([\\s\\S]+)' at ([\\s\\S]+)$");
				break;
			case "Execute":
				sectionStart = message
						.matches("^Begin: Execute SqlObj '([\\s\\S]+)' at ([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: execute SqlObj([\\s\\S]{0,3}) at ([\\s\\S]+)$");
				break;
			case "NamedSearch":
				sectionStart = message
						.matches("^Begin: SetNamedSearch for SqlObj '([\\s\\S]+)' at ([\\s\\S]+), Named Search: '([\\s\\S]+)' - '([\\s\\S]*)'$");
				sectionEnd = message
						.matches("^End: SetNamedSearch for SqlObj([\\s\\S]{0,3}) at ([\\s\\S]+)$");
				break;
			case "Sort":
				sectionStart = message
						.matches("^Begin: SetSortSpec for SqlObj '([\\s\\S]+)' at ([\\s\\S]+), Sort Spec: '([\\s\\S]*)'$");
				sectionEnd = message
						.matches("^End: SetSortSpec for SqlObj([\\s\\S]{0,3}) at ([\\s\\S]+)$");
				break;
			case "WriteRecord":
				sectionStart = message
						.matches("^Begin: WriteRecord for SqlObj '([\\s\\S]+)' at ([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: WriteRecord for SqlObj([\\s\\S]{0,3}) at ([\\s\\S]+)$");
				break;
			}
			break;
		case "ObjMgrSqlLog":
			switch (subType) {
			case "SqlTag":
				sectionStart = message
						.matches("^Begin: Execute SqlObj '([\\s\\S]+)' at ([\\s\\S]+) with SqlTag=([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: Execute SqlObj with SqlTag$");
				break;
			}
			break;
		case "ObjMgrBusCompLog":
			switch (subType) {
			case "Create":
				sectionStart = message
						.matches("^Begin: construct BusComp \"([\\s\\S]+)\" at ([\\s\\S]+)$");
				sectionEnd = message
						.matches("^End: construct BusComp \"([\\s\\S]+)\" at ([\\s\\S]+)$");
				break;
			}
			break;
		}
	}

	public LogLine(String initialString, long lineId, String delimiter) {
		String[] tokenList = initialString.split(delimiter);
		DateTimeFormatter dateFormat = DateTimeFormatter
				.ofPattern("yyyy-MM-dd HH:mm:ss");
		Matcher matcher = Pattern.compile("^.* at ([\\w])+$").matcher(
				initialString);
		if (tokenList.length >= 6) {
			id = lineId;
			type = tokenList[0];
			subType = tokenList[1];
			severity = Integer.parseInt(tokenList[2]);
			sarmId = tokenList[3];
			date = LocalDateTime.parse(tokenList[4], dateFormat);
			message = tokenList[5];
			;
			extraInfo = "";
			address = "";
			internalType = "";
			sectionStart = false;
			sectionEnd = false;
			items = new ArrayList<LogLine>();
			getLineAction();
			if (matcher.find()) {
				address = matcher.group(1);
			}
			for (int i = 6; i < tokenList.length; i++) {
				extraInfo += tokenList[i];
			}
			warning = subType.indexOf("Warning") > -1;
			error = subType.indexOf("Error") > -1;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LogLine [type=" + type + ", subType=" + subType + ", message="
				+ message + ", children=" + items.size() + "]";
	}
}
