package com.extremumlogica.siebellog.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Workflow {
	private String name;
	private Map<String,String> processProperties;
	private ArrayList<WorkflowStep> steps;
	private ArrayList<LogLine> items;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the inputs
	 */
	public Map<String, String> getProcessProperties() {
		return processProperties;
	}

	/**
	 * @param inputs the inputs to set
	 */
	public void setProcessProperties(Map<String, String> inputs) {
		this.processProperties = inputs;
	}

	/**
	 * @return the steps
	 */
	public ArrayList<WorkflowStep> getSteps() {
		return steps;
	}

	/**
	 * @param steps the steps to set
	 */
	public void setSteps(ArrayList<WorkflowStep> steps) {
		this.steps = steps;
	}

	/**
	 * @return the items
	 */
	public ArrayList<LogLine> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<LogLine> items) {
		this.items = items;
	}

	public Workflow(LogLine line) {
		String propName = "";
		String type = "";
		String subType = "";
		String msg = line.getMessage();
		name = msg.substring(msg.indexOf('\'')+1,msg.lastIndexOf('\''));
		processProperties = new HashMap<String, String>();
		steps = new ArrayList<WorkflowStep>();
		items = new ArrayList<LogLine>();
		for (LogLine childLine : line.getItems()) {
			type = childLine.getType();
			subType = childLine.getSubType();
			msg = childLine.getMessage();
			switch (type) {
			case "PrcExec":
				if(subType.matches("PropSet")){
					int start = msg.indexOf("Name: '");
					int end = msg.indexOf("' Datatype:");
					boolean isName = start >= 0 && end >= 0 && end > start;
					if(propName.isEmpty()){
						if(isName){
							propName = msg.substring(start+7, end);
						}
					}else{
						if(isName){
							msg = "";
						}
						processProperties.put(propName, msg);
						propName = isName ? childLine.getMessage().substring(start+7, end) :"";
					}
				}
				break;
			case "StpExec":
				steps.add(new WorkflowStep(childLine));
				break;
			default:
				items.add(childLine);
				break;
			}
		}
	}

}
