package com.extremumlogica.siebellog.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkflowStep {
	private String name;
	private String input;
	private String output;
	private String task;
	private String condition;
	private ArrayList<LogLine> items;
	private Map<String,String> processProperties;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the input
	 */
	public String getInput() {
		return input;
	}

	/**
	 * @param input the input to set
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * @return the output
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * @param output the output to set
	 */
	public void setOutput(String output) {
		this.output = output;
	}

	/**
	 * @return the task
	 */
	public String getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(String task) {
		this.task = task;
	}

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the items
	 */
	public ArrayList<LogLine> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<LogLine> items) {
		this.items = items;
	}

	/**
	 * @return the processProperties
	 */
	public Map<String, String> getProcessProperties() {
		return processProperties;
	}

	/**
	 * @param processProperties the processProperties to set
	 */
	public void setProcessProperties(Map<String, String> processProperties) {
		this.processProperties = processProperties;
	}

	public WorkflowStep(LogLine line) {
		processProperties = new HashMap<String, String>();
		items = new ArrayList<LogLine>();
		String propName = "";
		String type = "";
		String subType = "";
		String msg = line.getMessage();
		name = msg.substring(msg.indexOf('\'')+1,msg.lastIndexOf('\''));
		for (LogLine childLine : line.getItems()) {
			type = childLine.getType();
			subType = childLine.getSubType();
			msg = childLine.getMessage();
			switch (type) {
			case "PrcExec":
				if(subType.matches("PropSet")){
					int start = msg.indexOf("Name: '");
					int end = msg.indexOf("' Datatype:");
					boolean isName = start >= 0 && end >= 0 && end > start;
					if(propName.isEmpty()){
						if(isName){
							propName = msg.substring(start+7, end);
						}
					}else{
						if(isName){
							msg = "";
						}
						processProperties.put(propName, msg);
						propName = isName ? childLine.getMessage().substring(start+7, end) :"";
					}
				}
				break;
			case "StpExec":
				switch (subType) {
				case "Task":
					task = msg;
					break;
				case "TaskArg":
					if(msg.indexOf("Input: ") == 0){
						input = msg.substring(("Input: ").length());
					}
					if(msg.indexOf("Output: ") == 0){
						output = msg.substring(("Output: ").length());
					}
					break;
				case "End":
					break;
				default:
					items.add(childLine);
					break;
				}
				break;
			default:
				items.add(childLine);
				break;
			}
		}
	}

}
