package com.extremumlogica.siebellog.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.extremumlogica.siebellog.dto.LogLine;
import com.extremumlogica.siebellog.dto.Workflow;
import com.google.gson.Gson;

public class LogLineController {

	private String logDelimiter = "\t";
	private final ArrayList<LogLine> root;
	private final ArrayList<Workflow> workflows;
	private final Map<Long, LogLine> tmpWorkflows;
	private final Stack<LogLine> parents;
	private boolean skipCache = true;
	private boolean skipFetchSQL = true;
	private boolean skipXMLCnvParserDebug = true;
	private boolean skipDfnLoad = true;
	private boolean skipSisnTcpIp = true;
	private boolean skipObjMgrQueryLogCache = true;
	private boolean skipMsgFlowDetail = true;
	private boolean skipNameServerLayerLog = true;
	private boolean skipSessMgrMsgSend = true;
	private boolean skipSARMLog = true;
	private boolean skipAUDITLog = true;
	private boolean skipFDRLog = true;
	private boolean rootProcessed = false;
	private int pageSize = 20;

	/**
	 * @return the page count
	 */
	public int getPageCount() {
		return root.isEmpty() ? 0 : root.size() / pageSize;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the logDelimiter, usually TAB character.
	 */
	public String getLogDelimiter() {
		return logDelimiter;
	}

	/**
	 * @return the logLines
	 */
	public ArrayList<LogLine> getLogLines() {
		return root;
	}

	/**
	 * @return if we are skipping "Cache" events
	 */
	public boolean isSkipCache() {
		return skipCache;
	}

	public void setSkipCache(boolean skipCache) {
		this.skipCache = skipCache;
	}

	/**
	 * @return if we are skipping "FetchSQL" events
	 */
	public boolean isSkipFetchSQL() {
		return skipFetchSQL;
	}

	public void setSkipFetchSQL(boolean skipFetchSQL) {
		this.skipFetchSQL = skipFetchSQL;
	}
	
	/**
	 * @return if we are skipping "XMLCnvParserDebug" events
	 */
	public boolean isSkipXMLCnvParserDebug() {
		return skipXMLCnvParserDebug;
	}

	/**
	 * @param set if we are skipping "XMLCnvParserDebug" events
	 */
	public void setSkipXMLCnvParserDebug(boolean skipXMLCnvParserDebug) {
		this.skipXMLCnvParserDebug = skipXMLCnvParserDebug;
	}
	
	/**
	 * @return if we are skipping "DfnLoad" events
	 * that are describing which parts of the workflow
	 * definition was loaded. Ex.: Steps, Connectors, Conditions, etc.
	 */
	public boolean isSkipDfnLoad() {
		return skipDfnLoad;
	}

	/**
	 * @param set if we are skipping "DfnLoad" events
	 * that are describing which parts of the workflow
	 * definition was loaded. Ex.: Steps, Connectors, Conditions, etc.
	 */
	public void setSkipDfnLoad(boolean skipDfnLoad) {
		this.skipDfnLoad = skipDfnLoad;
	}
	
	/**
	 * @return if we are skipping SisnTcpIp events
	 */
	public boolean isSkipSisnTcpIp() {
		return skipSisnTcpIp;
	}

	/**
	 * @param if we are skipping SisnTcpIp events
	 */
	public void setSkipSisnTcpIp(boolean skipSisnTcpIp) {
		this.skipSisnTcpIp = skipSisnTcpIp;
	}

	/**
	 * @return if we are skipping skipObjMgrQueryLog Cache events
	 */
	public boolean isSkipObjMgrQueryLogCache() {
		return skipObjMgrQueryLogCache;
	}

	/**
	 * @param if we are skipping skipObjMgrQueryLog Cache events
	 */
	public void setSkipObjMgrQueryLogCache(boolean skipObjMgrQueryLogCache) {
		this.skipObjMgrQueryLogCache = skipObjMgrQueryLogCache;
	}

	/**
	 * @return if we are skipping "MessageFlow" - "MsgFlowDetail" events
	 */
	public boolean isSkipMsgFlowDetail() {
		return skipMsgFlowDetail;
	}

	/**
	 * @param if we are skipping "MessageFlow" - "MsgFlowDetail" events
	 */
	public void setSkipMsgFlowDetail(boolean skipMsgFlowDetail) {
		this.skipMsgFlowDetail = skipMsgFlowDetail;
	}

	/**
	 * @return if we are skipping "NameServerLayerLog" events
	 */
	public boolean isSkipNameServerLayerLog() {
		return skipNameServerLayerLog;
	}

	/**
	 * @param if we are skipping "NameServerLayerLog" events
	 */
	public void setSkipNameServerLayerLog(boolean skipNameServerLayerLog) {
		this.skipNameServerLayerLog = skipNameServerLayerLog;
	}

	/**
	 * @return if we are skipping "SessMgr" - "MsgSend" events
	 */
	public boolean isSkipSessMgrMsgSend() {
		return skipSessMgrMsgSend;
	}

	/**
	 * @param if we are skipping "SessMgr" - "MsgSend" events
	 */
	public void setSkipSessMgrMsgSend(boolean skipSessMgrMsgSend) {
		this.skipSessMgrMsgSend = skipSessMgrMsgSend;
	}

	/**
	 * @return if we are skipping "SARMLog" events
	 */
	public boolean isSkipSARMLog() {
		return skipSARMLog;
	}

	/**
	 * @param if we are skipping "SARMLog" events
	 */
	public void setSkipSARMLog(boolean skipSARMLog) {
		this.skipSARMLog = skipSARMLog;
	}

	/**
	 * @return if we are skipping audit logs
	 */
	public boolean isSkipAUDITLog() {
		return skipAUDITLog;
	}

	/**
	 * @param if we are skipping audit logs
	 */
	public void setSkipAUDITLog(boolean skipAUDITLog) {
		this.skipAUDITLog = skipAUDITLog;
	}

	/**
	 * @return if we are skipping "FDRLog" events
	 */
	public boolean isSkipFDRLog() {
		return skipFDRLog;
	}

	/**
	 * @param if we are skipping "FDRLog" events
	 */
	public void setSkipFDRLog(boolean skipFDRLog) {
		this.skipFDRLog = skipFDRLog;
	}

	public LogLineController() throws Exception {
		root = new ArrayList<LogLine>();
		workflows = new ArrayList<Workflow>();
		tmpWorkflows = new HashMap<Long, LogLine>();
		parents = new Stack<LogLine>();
	}

	public boolean loadFile(BufferedReader reader) {
		boolean result = false;
		root.clear();
		tmpWorkflows.clear();
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				if (!line.isEmpty()) {
					if (line.indexOf('/') != 0) {
						addLine(line);
					}
				}
			}
			if (!root.isEmpty()) {
				result = true;
			}
		} catch (IOException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	private LogLine getLastElement(ArrayList<LogLine> logLineList) {
		return logLineList.size() > 0 ? logLineList.get(logLineList.size() - 1)
				: null;
	}

	private boolean processRootLine(String line) {
		boolean result = false;
		String[] root = line.split(" ");
		if (root.length > 11) {
			logDelimiter = Character
					.toString((char) Integer.parseInt(root[10]));
			result = true;
		}
		return result;
	}

	private boolean allowLine(String logLine) {
		boolean result = true;
		if ((skipCache && logLine.contains("CchMgrLog\t"))
				|| (skipFetchSQL && logLine.contains("Sql Cursor"))
				|| (skipXMLCnvParserDebug && logLine.contains("XMLConversion\tXMLCnvParserDebug\t"))
				|| (skipDfnLoad && logLine.contains("DfnLoad\t"))
				|| (skipObjMgrQueryLogCache && logLine.contains("ObjMgrQueryLog\tCache\t"))
				|| (skipSisnTcpIp && logLine.contains("SisnTcpIp\t"))
				|| (skipMsgFlowDetail && logLine.contains("MessageFlow\tMsgFlowDetail\t"))
				|| (skipNameServerLayerLog && logLine.contains("NameServerLayerLog\t"))
				|| (skipSARMLog && logLine.contains("SARMLog\t"))
				|| (skipAUDITLog && logLine.contains("[AUDIT_LOG]"))
				|| (skipFDRLog && logLine.contains("FDRLog\t"))
				|| (skipSessMgrMsgSend && logLine.contains("SessMgr\tMsgSend\t"))) {
			result = false;
		}
		return result;
	}

	private void addLine(String logLine) {
		LogLine logObj;
		int counter = 0;
		if (!rootProcessed) {
			rootProcessed = processRootLine(logLine);
		} else {
			if (allowLine(logLine)) {
				for (int i = 0; i < logLine.length(); i++) {
					if (logLine.charAt(i) == logDelimiter.charAt(0)) {
						counter++;
					}
				}
				if (counter == 5) {
					logObj = new LogLine(logLine, root.size(), logDelimiter);
					if (logObj.getType() != null) {
						if (logObj.isError() && parents.size() > 0) {
							parents.peek().setError(true);
						}
						if (logObj.isWarning() && parents.size() > 0) {
							parents.peek().setWarning(true);
						}
						if (logObj.isSectionStart()) {
							parents.push(logObj);
							if (logObj.getInternalType() == "workflow") {
								if(!tmpWorkflows.containsKey(logObj.getId())){
									tmpWorkflows.put(new Long(logObj.getId()),logObj);
								}
							}
						} else if (logObj.isSectionEnd()) {
							if (parents.size() > 0) {
								parents.peek().getItems().add(logObj);
								if (parents.size() == 1) {
									root.add(parents.pop());
								} else {
									LogLine tmpLine = parents.pop();
									parents.peek().getItems().add(tmpLine);
								}

							} else {
								root.add(logObj);
							}
						} else {
							if (parents.size() > 0) {
								parents.peek().getItems().add(logObj);
							} else {
								root.add(logObj);
							}
						}
					}
				} else {
					logObj = getLastElement(root);
					if (logObj != null) {
						logObj.setExtraInfo(logObj.getExtraInfo().isEmpty() ? logLine
								: logObj.getExtraInfo() + '\n' + logLine);
					}
				}
			}
		}
	}

	public String getJsonPage(int pageNum) {
		String result = "";
		if (pageNum >= 0 && pageNum <= getPageCount()) {
			ArrayList<LogLine> tmpData = new ArrayList<LogLine>();
			Gson gson;
			int firstElement = pageNum * pageSize;
			int lastElement = (pageNum + 1) * pageSize;
			if (lastElement > root.size()) {
				lastElement = root.size() - 1;
			}
			if (firstElement >= 0 && lastElement < root.size()) {
				for (int i = firstElement; i < lastElement; i++) {
					tmpData.add(root.get(i));
				}
				if (!root.isEmpty()) {
					gson = new Gson();
					result = gson.toJson(tmpData);
				}
			}
		}
		return result;
	}

	public String getWorkflows() {
		String result = "";
		if (!tmpWorkflows.isEmpty()) {
			Gson gson;
			gson = new Gson();
			result = gson.toJson(tmpWorkflows.values());
		}
		return result;
	}
	
	
	public String getProcessedWorkflows(){
		String result = "";
		processWorkflows();
		if(workflows.size() > 0){
			Gson gson;
			gson = new Gson();
			result = gson.toJson(workflows);
		}
		return result;
	}
	
	private void processWorkflows(){
		if(!tmpWorkflows.isEmpty()){
			for (LogLine logLine : tmpWorkflows.values()) {
				workflows.add(new Workflow(logLine));
			}
		}
	}
	
}
