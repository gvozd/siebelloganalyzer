<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="logController" class="com.extremumlogica.siebellog.helpers.LogLineController" scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/demos.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="jqwidgets/jqxtree.js"></script>
<title>Insert title here</title>
</head>
<body>
<div id="kokoko"></div>
<script>
$(function() {
	var wfData = <%=logController.getProcessedWorkflows()%>;
	var source =
    {
        datatype: "json",
        datafields: [
            { name: 'name', type: 'string' },
            { name: 'steps', type: 'array' },
            { name: 'processProperties', type: 'object' }
        ],
        localData : JSON.parse(data)
    };
	var dataAdapter = new $.jqx.dataAdapter(source);
});
</script>
</body>
</html>