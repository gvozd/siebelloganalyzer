<%@page import="java.util.Collection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="logController"
	class="com.extremumlogica.siebellog.helpers.LogLineController"
	scope="session" />
<%
	PrintWriter output = response.getWriter();
	InputStream fileContent = request.getInputStream();
	try (BufferedReader reader = new BufferedReader(
			new InputStreamReader(fileContent, "UTF-8"))) {
		output.print(logController.loadFile(reader) == true ? logController
				.getPageCount() : 0);
	} catch (Exception e) {
		output.print("error");
	} finally {
		if (fileContent != null) {
			fileContent.close();
		}
		if (output != null) {
			output.close();
		}
	}
%>
