<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="logController"
	class="com.extremumlogica.siebellog.helpers.LogLineController"
	scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="jqwidgets/jqxtreegrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxtoolbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxinput.js"></script>
<script type="text/javascript" src="jqwidgets/jquery.fileupload.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="jqwidgets/styles/jquery.fileupload.css" type="text/css" />
<link rel="stylesheet" href="jqwidgets/styles/gridcells.css" type="text/css" />
<title>Insert title here</title>

</head>
<body>
	<div id="jqxToolBar"></div>
	<div id='jqxTree'></div>
	<script type="text/javascript">
		var currentPage = 0;
		var pageCount = 0;

		function getPreviousPage() {
			if (currentPage > 0) {
				currentPage--;
				getPageData(currentPage);
			}
		}
		function getNextPage() {
			if (currentPage < pageCount) {
				currentPage++;
				getPageData(currentPage);
			}
		}
		function rowDetailsRenderer(rowKey, row) {
			var indent = (1 + row.level) * 20;
			var details = row.extraInfo === "" ? null
					: "<table style='margin: 10px; margin-left: " + indent + "px;'><tr><td>"
							+ row.extraInfo + "</td></tr></table>";
			if (details != null) {
				details.jqxExpander();
			}
			return details;
		}
		function pagerRenderer() {
			var element = $("<div style='margin-left: 10px; margin-top: 5px; width: 100%; height: 100%;'></div>");
			var leftButton = $("<div style='padding: 0px; float: left;'><div style='margin-left: 9px; width: 16px; height: 16px;'></div></div>");
			var rightButton = $("<div style='padding: 0px; margin: 0px 3px; float: left;'><div style='margin-left: 9px; width: 16px; height: 16px;'></div></div>");
			var currentPageItem = $("<div style='padding: 0px; margin: 0px 3px; float: left;'><input type=”text” id=”pageInput” style='margin-left: 9px; width: 64px; height: 16px;'/></div>");
			var totalPagesItem = $("<div style='padding: 0px; margin: 0px 3px; float: left;'><div style='margin-left: 9px; width: 16px; height: 16px;'></div></div>");
			leftButton.find('div').addClass('jqx-icon-arrow-left');
			leftButton.width(36);
			leftButton.jqxButton();
			rightButton.find('div').addClass('jqx-icon-arrow-right');
			rightButton.width(36);
			rightButton.jqxButton();
			currentPageItem.jqxInput({
				placeHolder : currentPage
			});
			totalPagesItem.text(pageCount);
			leftButton.appendTo(element);
			currentPageItem.appendTo(element);
			totalPagesItem.appendTo(element);
			rightButton.appendTo(element);
			// update buttons states.
			var handleStates = function(event, button, className, add) {
				button.on(event, function() {
					if (add == true) {
						button.find('div').addClass(className);
					} else
						button.find('div').removeClass(className);
				});
			};
			rightButton.click(getNextPage);
			leftButton.click(getPreviousPage);
			return element;
		}
		function getPageData(e) {
			var cellClass = function(row, dataField, cellText, rowData) {
				if (rowData['error'] == true) {
					return "logError";
				}
				if (rowData['warning'] == true) {
					return "logWarning";
				}
			};
			$.get("getJSONPage.jsp", {
				pageNum : e,
			}, function(data, textStatus) {
				if (textStatus === "success") {
					var source = {
						dataType : "json",
						dataFields : [ {name : 'id', type : 'number'}, 
						               {name : 'type', type : 'string'}, 
						               {name : 'subType', type : 'string'}, 
						               {name : 'message', type : 'string'}, 
						               {name : 'extraInfo', type : 'string'}, 
						               {name : 'items', type : 'array'}, 
						               {name : 'error', type : 'boolean'}, 
						               {name : 'warning', type : 'boolean'} ],
						hierarchy : {root : 'items'},
						localData : JSON.parse(data)
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$('#jqxTree').jqxTreeGrid({
						source : dataAdapter,
						width : '100%',
						pageable : true,
						pageSizeMode: 'root',
		                pageSize: 20,
						pagerRenderer : pagerRenderer,
						rowDetails : false,
						rowDetailsRenderer : rowDetailsRenderer,
						columnsResize : true,
						columns : [ {text : 'Id', dataField : 'id', width : '10%', cellClassName : cellClass},
						            {text : 'Type', dataField : 'type', width : '10%', cellClassName : cellClass}, 
						            {text : 'SubType', dataField : 'subType', width : '10%', cellClassName : cellClass}, 
						            {text : 'Message', dataField : 'message', width : '70%', cellClassName : cellClass} ]}).width('100%');
				}
			});
		}

		$(function() {
			$("#jqxToolBar").jqxToolBar({width : "100%", height : 35, tools : "button button", initTools : 
				function(type, index, tool, menuToolIninitialization) {
									switch (index) {
									case 0:
										tool.append('<span class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>Select file</span><input id="fileupload" type="file" name="files[]" multiple="false"></span>');
										$('#fileupload').fileupload({url : 'load.jsp', dataType : 'json', formAcceptCharset : 'UTF-8',done : 
											function(e, data) {
												if (e.type === "fileuploaddone"	&& data._response.textStatus === "success") {
													pageCount = data._response.result;
													getPageData(0);
												}
											}}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined	: 'disabled');
										break;
									case 1:
										tool.text("Get Workflows Only").click(function(){
											getPageData(-1);
										});
										break;
									}
								}
							});
		});
	</script>
</body>
</html>