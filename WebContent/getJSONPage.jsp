<%@page import="java.util.Map"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="logController"
	class="com.extremumlogica.siebellog.helpers.LogLineController"
	scope="session"></jsp:useBean>
<%
PrintWriter output = response.getWriter();
Map<String, String[]> paramMap = request.getParameterMap();
if(paramMap.containsKey("pageNum")){
	int pageNum =  Integer.parseInt(request.getParameter("pageNum"));
	if(pageNum >= 0){
		   output.print(logController.getJsonPage(pageNum));
	}
	if(pageNum == -1){
		output.print(logController.getWorkflows());
	}
}
output.close();
%>
